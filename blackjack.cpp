#include <iostream>
#include <array>
#include <random>
#include <algorithm>
#include <limits>
#include <string>

enum class Rank {
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine,
    ten,
    jack,
    queen,
    king,
    ace,
    nRanks,
};

enum class Suit {
    club,
    diamond,
    heart,
    spade,
    nSuits,
};

struct Card {
    Suit suit {};
    Rank rank {};
};

void printCard(const Card& card) {
    switch (card.rank) {
        case Rank::two:
        case Rank::three:
        case Rank::four:
        case Rank::five:
        case Rank::six:
        case Rank::seven:
        case Rank::eight:
        case Rank::nine:
            std::cout << static_cast<char>('2'+static_cast<char>(card.rank));
            break;
        case Rank::ten:
            std::cout << 'T';
            break;
        case Rank::jack:
            std::cout << 'J';
            break;
        case Rank::queen:
            std::cout << 'Q';
            break;
        case Rank::king:
            std::cout << 'K';
            break;
        case Rank::ace: 
            std::cout << 'A';
            break;
        default:
            std::cout << '?';
            break;
    }
    switch (card.suit) {
        case Suit::club:
            std::cout << 'C';
            break;
        case Suit::diamond:
            std::cout << 'D';
            break;
        case Suit::heart:
            std::cout << 'H';
            break;
        case Suit::spade:
            std::cout << 'S';
            break;
        default:
            std::cout << '?';
            break;
    }


}


using Deck = std::array<Card, 52>;

Deck createDeck() {
    Deck deck {};
    for (std::size_t suit {0}; suit < static_cast<std::size_t>(Suit::nSuits); ++suit) {
        for (std::size_t rank {0}; rank < static_cast<std::size_t>(Rank::nRanks); ++rank) {
            Card& card {deck.at(suit*13+rank)};
            card.suit = static_cast<Suit>(suit);
            card.rank = static_cast<Rank>(rank);
        }
    }
    return deck;
}

void printDeck(const Deck& deck) {
    for (const auto& card : deck) {
        printCard(card);
        std::cout << ' ';
    }
    std::cout << '\n';
}

void shuffleDeck(Deck& deck) {
    static std::mt19937 mt {std::random_device{}()};
    std::shuffle(deck.begin(), deck.end(), mt);
}

int cardValue(const Card& card) {
    switch (card.rank) {
        case Rank::two:
        case Rank::three:
        case Rank::four:
        case Rank::five:
        case Rank::six:
        case Rank::seven:
        case Rank::eight:
        case Rank::nine:
            return 2+static_cast<int>(card.rank);
        case Rank::ten:
        case Rank::jack:
        case Rank::queen:
        case Rank::king:
            return 10;
        case Rank::ace: 
            return 11;
        default:
            return 0;
    }
}

struct Status {
    int score {};
    int nAces {};
};

void printStatus(const Status& player, const Status& dealer) {
    std::cout << "Player score: " << player.score << ' ';
    if (player.nAces) {
        std::cout << "with " << player.nAces << " ace ";
    }
    std::cout << '\n';

    std::cout << "Dealer score: " << dealer.score << ' ';
    if (dealer.nAces) {
        std::cout << "with " << dealer.nAces << " ace ";
    }
    std::cout << '\n';
}

bool addOneCard(Status& status, const Card& card) {
    constexpr int blackJack {21};
    status.score += cardValue(card);
    status.nAces += (card.rank == Rank::ace);
    if (status.score > blackJack && status.nAces) {
        // We have aces to shrink
        status.nAces -= 1;
        status.score -= 10;
    }
    return status.score > blackJack;
}


enum class Result {
    playerWin,
    dealerWin,
    tie,
};


Result playBlackJack(const Deck& deck) {
    Status dealer {};
    Status player {};
    addOneCard(dealer, deck.at(0));
    addOneCard(player, deck.at(1));
    addOneCard(player, deck.at(2));
    std::size_t currCard {3};
    while (true) {
        printStatus(player, dealer);
        std::cout << "hit (h) or stand (s)? ";
        char hitOrStand {};
        std::cin >> hitOrStand;
        if (hitOrStand == 's') {
            break;
        }
        if (hitOrStand != 'h') {
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            continue;
        }
        if (addOneCard(player, deck.at(currCard))) {
            std::cout << "player busted: " << player.score << '\n';
            return Result::dealerWin;
        }
        currCard += 1;
    }
    constexpr int dealerMin {17};
    while (dealer.score < dealerMin) {
        printStatus(player, dealer);
        if (addOneCard(dealer, deck.at(currCard))) {
            std::cout << "dealer busted: " << dealer.score << '\n';
            return Result::playerWin;
        }
        currCard += 1;
    }

    printStatus(player, dealer);
    if (player.score == dealer.score) {
        return Result::tie;
    } else if (player.score > dealer.score) {
        return Result::playerWin;
    } else {
        return Result::dealerWin;
    }
}

void printResult(Result result) {
    switch (result) {
        case Result::playerWin:
            std::cout << "Player win!\n";
            break;
        case Result::dealerWin:
            std::cout << "Dealer win!\n";
            break;
        case Result::tie:
            std::cout << "tie!\n";
            break;
        default:
            std::cout << "????\n";
            break;
    }
}

int main() {
    Deck deck {createDeck()};
    shuffleDeck(deck);
    printResult(playBlackJack(deck));
    return 0;
}
