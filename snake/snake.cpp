#include <cstddef>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdlib>
#include <queue>
#include <random>
#include <thread>
#include <chrono>
#include <cstdio>
#include <ncurses.h>


struct Ground {
    std::size_t width {};
    std::size_t height {};
    std::string ground {};
};


Ground initGround(std::size_t width, std::size_t height) {
    std::stringstream os {};
    std::string topAndBottom (width+2, '_');
    os << topAndBottom << '\n';
    std::string interior (width, ' ');
    for (std::size_t i {0}; i < height; ++i) {
        os << '|' << interior << "|\n";
    }
    os << topAndBottom << '\n';
    return {width, height, os.str()};
}

void printGround(const Ground& ground) {
    mvprintw(0, 0, "%s", ground.ground.c_str());
    refresh();
}

struct Coordinate {
    std::size_t x {};
    std::size_t y {};
};

bool operator!=(const Coordinate& a, const Coordinate& b) {
    return a.x != b.x || a.y != b.y;
}

bool operator==(const Coordinate& a, const Coordinate& b) {
    return a.x == b.x && a.y == b.y;
}

void addHead(Ground& ground, const Coordinate& head) {
    std::size_t nCols {ground.width+3}; // Edges and \n
    ground.ground.replace((head.y+1)*nCols+head.x+1, 1, "O");
}

void removeTail(Ground& ground, const Coordinate& tail) {
    std::size_t nCols {ground.width+3}; // Edges and \n
    ground.ground.replace((tail.y+1)*nCols+tail.x+1, 1, " ");
}

void addFruit(Ground& ground, const Coordinate& fruit) {
    std::size_t nCols {ground.width+3}; // Edges and \n
    ground.ground.replace((fruit.y+1)*nCols+fruit.x+1, 1, "X");
}

enum class Direction {
    right,
    left,
    up,
    down,
};

struct Snake {
    std::deque<Coordinate> coords {};
    Direction direction {Direction::right};
};

Snake initSnake(Ground& ground) {
    Snake snake {};
    constexpr std::size_t initSnakeLength {5};
    for (std::size_t i {0}; i < initSnakeLength; ++i) {
        snake.coords.push_back({i, 0});
        addHead(ground, {i, 0});
    }
    return snake;
}

bool checkNewHeadInRange(const Snake& snake, const Ground& ground) {
    const Coordinate head {snake.coords.back()};
    switch (snake.direction) {
        case Direction::right: 
            return head.x+1 < ground.width;
        case Direction::left: 
            return static_cast<int>(head.x-1) >= 0;
        case Direction::up: 
            return static_cast<int>(head.y-1) >= 0;
        case Direction::down: 
            return head.y+1 < ground.height;
        default:
            // TODO: need default case
            return false;
    }
}

bool checkEatSelf(const Snake& snake, const Coordinate& newHead) {
    for (auto coord : snake.coords) {
        if (newHead == coord) {
            return true;
        }
    }
    return false;
}

Coordinate getNewHead(const Snake& snake) {
    const Coordinate head {snake.coords.back()};
    switch (snake.direction) {
        case Direction::right: 
            return {head.x+1, head.y};
        case Direction::left: 
            return {head.x-1, head.y};
        case Direction::up: 
            return {head.x, head.y-1};
        case Direction::down: 
            return {head.x, head.y+1};
        default:
            // TODO: need default case
            return {0, 0};
    }
}

void updateSnake(Snake& snake, Ground& ground, const Coordinate& newHead, const Coordinate& fruit) {
    snake.coords.push_back(newHead);
    addHead(ground, newHead);
    if (newHead != fruit) {
        removeTail(ground, snake.coords.front());
        snake.coords.pop_front();
    }
    
    

}

constexpr std::size_t width {20};
constexpr std::size_t height {20};

Coordinate generateFruit() {
    static std::mt19937 mt {std::random_device{}()};
    static std::uniform_int_distribution<std::size_t> x_dist {0, width-1};
    static std::uniform_int_distribution<std::size_t> y_dist {0, height-1};
    return {x_dist(mt), y_dist(mt)};
}

constexpr int msPerFrame {200};

void handleEventAndWait(Snake& snake) {
    auto now {std::chrono::system_clock::now()};
    switch (getch()) {
        case KEY_LEFT:
            if (snake.direction != Direction::right) {
                snake.direction = Direction::left;
            }
            break;
        case KEY_RIGHT:
            if (snake.direction != Direction::left) {
                snake.direction = Direction::right;
            }
            break;
        case KEY_UP:
            if (snake.direction != Direction::down) {
                snake.direction = Direction::up;
            }
            break;
        case KEY_DOWN:
            if (snake.direction != Direction::up) {
                snake.direction = Direction::down;
            }
            break;
        default:
            break;
    }
    std::this_thread::sleep_until(now+std::chrono::milliseconds(msPerFrame));

}



int main() {
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, true);  // This initialization is for arrow key to work
    timeout(msPerFrame);
    Ground ground {initGround(width, height)};
    Snake snake {initSnake(ground)};
    Coordinate fruit {generateFruit()};
    addFruit(ground, fruit);
    while (true) {
        printGround(ground);
        handleEventAndWait(snake);
        if (!checkNewHeadInRange(snake, ground))  {
            break;
        }
        Coordinate newHead {getNewHead(snake)};
        if (checkEatSelf(snake, newHead)) {
            break;
        }
        updateSnake(snake, ground, newHead, fruit);
        if (newHead == fruit) {
            fruit = generateFruit();
            addFruit(ground, fruit);
        }
    }
    endwin();
    return 0;
}
